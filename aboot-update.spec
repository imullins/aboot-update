Name:           aboot-update
Version:        0.7.6
Release:        1%{?dist}
Summary:        Update aboot from kernel/initrd

License:        GPLv2+
Source1:        aboot-update
Source2:        90-aboot.install

Requires:       android-tools
Requires:       aboot-deploy
Requires:       unzboot
Requires:       file

%description

Aboot-update is a tool that given a kernel version creates an android
boot image (aboot) file called /boot/aboot-VERSION.img based on the
options specified in /boot/aboot.cfg.

There is also a kernel-install script (90-aboot.install) that automatically
runs this on each new kernel install if the kernel install layout is set
to aboot.

%prep
rm -rf %{name}-%{version}
mkdir %{name}-%{version}


%build
cd %{name}-%{version}

%install
cd %{name}-%{version}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_prefix}/lib/kernel/install.d
install -m755 %{SOURCE1} %{buildroot}%{_bindir}/aboot-update
install -m755 %{SOURCE2} %{buildroot}%{_prefix}/lib/kernel/install.d/90-aboot.install

%files
%{_bindir}/aboot-update
%{_prefix}/lib/kernel/install.d/90-aboot.install

%changelog
* Wed Apr 17 2024 Ian Mullins <imullins@redhat.com>
- Remove hard link to aboot.img.

* Thu Apr 11 2024 Eric Curtin <ecurtin@redhat.com>
- Add aboot-deploy step for kernel upgrades.

* Tue Mar 26 2024 Eric Curtin <ecurtin@redhat.com>
- BLS file format changed in ostree.

* Wed Mar 20 2024 Eric Curtin <ecurtin@redhat.com>
- If kernel is larger than 40M, use compression, bug fix for
  earlier change.

* Sat Mar 16 2024 Eric Curtin <ecurtin@redhat.com>
- Skip mkbootimg generation on second call of aboot-update in ostree
  builds

* Wed Mar 13 2024 Brian Masney <bmasney@redhat.com>
- Add file package as a dependency.

* Thu Feb 22 2024 Ian Mullins <imullins@redhat.com>
- Increase kernel size threshold for compression to 40MB.

* Mon Dec 18 2023 Brian Masney <bmasney@redhat.com>
- Add support for kernels built using the vmlinuz.efi upstream kernel
  build target. This is how the Fedora kernel is built on aarch64.
- Remove abootimg dependency

* Wed Sep 13 2023 Eric Curtin <ecurtin@redhat.com>
- Make the package buildable on Fedora.

* Wed Aug 30 2023 Brian Masney <bmasney@redhat.com>
- Don't decompress the kernel for the debug variant since it makes
  the overall image too large to fit onto the boot partitions.

* Tue Aug 22 2023 Brian Masney <bmasney@redhat.com>
- Decompress the kernel to help with boot time.
- Various shellcheck cleanups.

* Fri Jul 21 2023 Eric Curtin <ecurtin@redhat.com>
- we now use androidboot.slot_suffix= to check if we are aboot
  or not and ostree= to parse the stateroot.

* Sat Jul 15 2023 Eric Curtin <ecurtin@redhat.com>
- Add mkbootimg support and some initial boilerplate for
  vbmeta

* Tue Jul 04 2023 Eric Curtin <ecurtin@redhat.com>
- Substitute ostree= with ostree=aboot for this case, so
  ostree knows how to boot correctly.

* Mon Mar 20 2023 Eric Curtin <ecurtin@redhat.com>
- Changes to allow for relative paths and absolute paths when
  specifying dtbs

* Tue Mar 14 2023 Eric Curtin <ecurtin@redhat.com>
- try to create both links

* Mon Mar 13 2023 Eric Curtin <ecurtin@redhat.com>
- extra link to satisfy osbuild

* Wed Jan 11 2023 Eric Curtin <ecurtin@redhat.com>
- ostree enablement

* Fri Aug 19 2022 Alexander Larsson <alexl@redhat.com>
- Initial version
